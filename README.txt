;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; CCK Private Fields
;;
;; Module Author   : markus_petrux (http://drupal.org/user/39593)
;; Module co-author: pcambra       (http://drupal.org/user/122101)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

The MAIN branch for this project is intentionally empty. Development and stable
releases can be found by checking out from the proper branch.
